
function gradeCalculator() {
    let userInput = document.getElementById("grade").value
    if (userInput >= 90) {
        alert("Grade is A+!")
    } else if (userInput >= 80) {
        alert("Grade is A!")
    } else if (userInput >= 70) {
        alert("Grade is B+!")
    } else if (userInput >= 60) {
        alert("Grade is B!")
    } else if (userInput >= 50) {
        alert("Grade is C+!")
    } else {
        alert("FAILED!")
    }
}